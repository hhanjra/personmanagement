package com.person.controller;

import com.person.DTO.Person;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@CrossOrigin(origins="*")
@RestController
@RequestMapping("/person")
public class PersonController {

    private List<Person> personList = createPersonList();

    @GetMapping(value = "/list")
    public List<Person> getAllPerson() {
       return personList;
    }

    @PostMapping(value = "/save")
    public Person savePerson(@RequestBody Person person) {
        personList.add(person);
        return person;
    }

    @GetMapping(value = "/search/{firstName}")
    public Person search(@PathVariable String firstName) {
       return personList.stream()
                .filter(s -> s.getFirstName().equalsIgnoreCase(firstName)).
                findFirst().orElse(null);
    }

    private static List<Person> createPersonList() {
        List<Person> list = new ArrayList<>();
        list.add(new Person("Harpreet", "Hanjra"));
        list.add(new Person("Rob", "Shilling"));
        list.add(new Person("Thomas", "James"));
        list.add(new Person("Jacob", "Ku"));
        list.add(new Person("Liam", "Zack"));
        list.add(new Person("Nathan", "Samuel"));
        return list;
    }
}
